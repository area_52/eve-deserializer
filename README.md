# EVE YAML Deserializer

Este proyecto fue creado ante la necesidad de transformar la información (de YAML a MSSQL Server) compartida por parte de [CCP Games](https://www.ccpgames.com/) para posteriormente usarla en la generación de herramientas que agilicen y optimicen la generación de recursos dentro del juego [EVE Online](https://www.eveonline.com/). Éstos recursos se encuentran disponibles en el [repositorio oficial para desarrolladores](https://developers.eveonline.com/resource) de EVE Online.

Para este proyecto se hará uso de algunos de los `SDE` (Static Data Export), que son archivos en formato YAML que contienen información
estática de EVE. Más información sobre estos archivos en este [enlace](https://docs.esi.evetech.net/docs/sde_introduction.html).

### Modelo Entidad-Relación

Los archivos `SDE` cuentan con múltiples atributos, para este proyecto sólo usaré los representados en el modelo que se muestra a continuación. Cabe mencionar que para la representación del modelo se utilizó el lenguaje [DBML](https://www.dbml.org/docs/) (Database Markup Language)

> _**TIP:** Para poder visualizar mejor el modelo, se recomienda insertar el siguiente código en la herramienta [DB Diagram](https://dbdiagram.io)_.

```dbml
Project eve_materials {
  database_type: 'SQL Server'
  Note: 'Base de datos de materiales existentes en el juego'
}


Table EVE.Materials {
  id int [pk, not null]
  basePrice decimal(14,2)
  groupID int
  iconID int
  materialName varchar(255)
  materialDescription text
  mass float
  portionSize int
  published bit
  volume decimal(18,3)
}

Table EVE.Compositions {
  id int [pk, not null]
  parentMaterial int [not null]
  childMaterial int [not null]
  quantity double [default: 0.00]

  indexes {
    (parentMaterial, childMaterial)[unique]
  }
}

// Relaciones
Ref: EVE.Materials.id < EVE.Compositions.parentMaterial [delete: cascade]
Ref: EVE.Materials.id < EVE.Compositions.childMaterial [delete: cascade]
```

_**Authored by** [Ayataki Byte](https://evewho.com/character/2114967246)_
