﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using YamlDotNet.Serialization;
using YamlDotNet.RepresentationModel;
using System.IO;
using System.Data;
using System.Reflection;

namespace EveDeserializer
{

    public class EveMaterial
    {
        public int Id { get; set; }
        public decimal BasePrice { get; set; }
        public int GroupId { get; set; }
        public int IconId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Mass { get; set; }
        public int PortionSize { get; set; }
        public bool Published { get; set; }
        public double Volume { get; set; }

        public override string ToString()
        {
            return Name.ToString();
        }

    }

    public class EveMaterialComposition
    {

    }

    public class YamlDeserializer
    {

        public List<EveMaterial> ToList(string filePath)
        {
            // Abrir el archivo YAML
            // Crear una instancia de StreamReader para leer desde un archivo.
            // La declaración de 'using' cerrará el StreamReader
            using (var reader = new StreamReader(filePath))
            {
                // Cargar el stream YAML
                var yaml = new YamlStream();
                yaml.Load(reader);

                // Recuperar el primer documento del archivo YAML
                var document = yaml.Documents[0];

                // Recuperar el primer nodo raíz del documento
                var rootMaterials = (YamlMappingNode)document.RootNode;

                // Crear una lista de para almacenar objetos Material
                List<EveMaterial> materialList = new List<EveMaterial>();

                // Iterar en cada ID de Material mapeado
                foreach (var material in rootMaterials.Children)
                {
                    // Crear un objeto Material para almacenar información
                    EveMaterial objEveMaterial = new EveMaterial();

                    try
                    {
                        // Obtener y almacenar el valor de la llave en la instancia de Material
                        // La primera llave del árbol es el ID del Material
                        objEveMaterial.Id = int.Parse(((YamlScalarNode)material.Key).Value);

                        // Mapear la lista de atributos del Material
                        YamlMappingNode materialAttibutes = (YamlMappingNode)material.Value;

                        // Mapear el nombre en los idiomas disponibles
                        YamlMappingNode name = (YamlMappingNode)materialAttibutes.Children[new YamlScalarNode("name")];

                        // Mapear la descripción en los idiomas disponibles
                        YamlMappingNode description = (YamlMappingNode)materialAttibutes.Children[new YamlScalarNode("description")];

                        // Llenar el objeto 'Material'
                        objEveMaterial.BasePrice = decimal.Parse(Helper.GetAttributeValue(materialAttibutes, "basePrice"));
                        objEveMaterial.IconId = int.Parse(Helper.GetAttributeValue(materialAttibutes, "iconID"));
                        objEveMaterial.GroupId = int.Parse(Helper.GetAttributeValue(materialAttibutes, "groupID"));
                        objEveMaterial.Mass = float.Parse(Helper.GetAttributeValue(materialAttibutes, "mass"));
                        objEveMaterial.PortionSize = int.Parse(Helper.GetAttributeValue(materialAttibutes, "portionSize"));
                        objEveMaterial.Published = bool.Parse(Helper.GetAttributeValue(materialAttibutes, "published"));
                        objEveMaterial.Volume = double.Parse(Helper.GetAttributeValue(materialAttibutes, "volume"));
                        objEveMaterial.Name = Helper.GetAttributeValue(name, "en");
                        objEveMaterial.Description = Helper.GetAttributeValue(description, "en");

                        // Almacenar el objeto 'Material' en la lista
                        materialList.Add(objEveMaterial);

                    }
                    catch (KeyNotFoundException)
                    {
                        // Si crashea
                        // El material no cuenta con alguno de los atributos
                        continue;
                    }
                    catch (Exception anotherException)
                    {
                        Console.WriteLine($"{anotherException.Source}:\n{anotherException.Message}");
                    }
                }

                return materialList;

            }

        }

        public DataTable ToDataTable(string yamlFilePath)
        {
            List<EveMaterial> eveMaterials = ToList(yamlFilePath);
            // Inicializar DataTable
            // Asignar como nombre del DT el nombre
            DataTable dt = new DataTable(typeof(EveMaterial).Name);
            // Obtener todas las propiedades
            /*
             * Obtener una matriz de objetos PropertyInfo que representan las propiedades públicas de un Type.
             * typeof(T) obtiene el tipo de "T" como un objeto System.Type.
             * GetProperties es un método estático de la clase Type que devuelve una matriz de objetos PropertyInfo,
             *      que representan las propiedades de un tipo.
             * BindingFlags.Public es una bandera que indica que se deben incluir solo las propiedades públicas en la matriz devuelta.
             * BindingFlags.Instance es una bandera que indica que se deben incluir solo las propiedades de instancia en la matriz devuelta.
             * Propiedades de clase son aquellas que pertenecen a la clase, mientras que las propiedades de instancia son aquellas que 
             *      pertenecen a una instancia específica de la clase.
             * BindingFlags.Public | BindingFlags.Instance es una combinación de banderas que indica que se deben incluir solo las propiedades 
             * públicas de instancia en la matriz devuelta.
             */
            PropertyInfo[] properties = typeof(EveMaterial).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            
            // Agregar una columna por cada propiedad al DataTable
            foreach (PropertyInfo property in properties)
            {
                // El nombre de la columna es el nombre de la propiedad
                dt.Columns.Add(property.Name);
            }

            // Agregar una fila por cada elemento T al DataTable
            foreach (EveMaterial item in eveMaterials)
            {
                var values = new object[properties.Length];

                // Agregar información a cada columna
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(item);
                }

                dt.Rows.Add(values);
            }

            return dt;
        }
    }
}