﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using YamlDotNet.RepresentationModel;

namespace EveDeserializer
{
    public class Helper
    {
        public static string GetAttributeValue(YamlMappingNode mappingNode, string attributeName)
        {

            return mappingNode.Children[new YamlScalarNode(attributeName)].ToString();
        }
    }
}
